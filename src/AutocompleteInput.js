import React, { Component } from 'react';
import './AutocompleteInput.css';

// TODO: improvement: store autocomplete searches and check if we already have them before making the request
// TODO: improve accessibility

class AutocompleteInput extends Component {
  constructor(props) {
    super(props);

    this.keyDownListener = this.keyDownListener.bind(this);
    this.clickListener = this.clickListener.bind(this);
    this.handleKeyboardSelect = this.handleKeyboardSelect.bind(this);
    this.handleKeyboardDown = this.handleKeyboardDown.bind(this);
    this.handleKeyboardUp = this.handleKeyboardUp.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.successfulAutocomplete = this.successfulAutocomplete.bind(this);
    this.unsuccessfulAutocomplete = this.unsuccessfulAutocomplete.bind(this);
    this.pendingAutocomplete = this.pendingAutocomplete.bind(this);
    this.searchResultHandleClick = this.searchResultHandleClick.bind(this);
    this.generateListboxContents = this.generateListboxContents.bind(this);
    this.resetAutocompleteState = this.resetAutocompleteState.bind(this);

    this.TAB_KEY = 'Tab';
    this.DOWN_ARROW_KEY = 'ArrowDown';
    this.UP_ARROW_KEY = 'ArrowUp';
    this.ESC_KEY = 'Escape';
    this.ENTER_KEY = 'Enter';
    this.MAX_SEARCH_VALUES = 5;

    this.autocompleteInput = undefined;
    this.searchResultElements = []
    this.emptyState = {
      searchValues: [],
      showSearchResults: false,
      pendingSearch: false,
      successfulSearch: undefined,
      failedSearch: undefined,
      focusedSearchResult: undefined
    };
    this.state = this.emptyState;
  }

  componentDidMount() {
    // set up global listeners
    document.addEventListener('keydown', this.keyDownListener);
    document.addEventListener('click', this.clickListener);
  }

  componentWillUnmount() {
    // destroy global listeners
    document.removeEventListner('keydown', this.keyDownListener);
    document.removeEventListner('click', this.clickListener);
  }

  resetAutocompleteState() {
    this.setState(this.emptyState);
    this.autocompleteInput.focus();
  }

  successfulAutocomplete(results) {
    // show search results if there are any
    if(results.length) {
      this.setState({
        searchValues: results.slice(0, this.MAX_SEARCH_VALUES - 1),
        showSearchResults: true,
        pendingSearch: false,
        successfulSearch: true,
        failedSearch: false,
        focusedSearchResult: undefined
      });
    // do not show empty search results
    } else {
      this.setState({
        showSearchResults: false,
        pendingSearch: false,
        successfulSearch: true,
        failedSearch: false
      });
    }
  }

  unsuccessfulAutocomplete() {
    this.setState({
      showSearchResults: false,
      pendingSearch: false,
      successfulSearch: false,
      failedSearch: true
    });
  }

  pendingAutocomplete() {
    this.setState({
      showSearchResults: true,
      pendingSearch: true
    });
  }

  keyDownListener(e) {
    if(this.state.showSearchResults) {
      if(e.key === this.ESC_KEY) {
        this.resetAutocompleteState();
        e.preventDefault();
        e.stopPropagation();
      } else if(e.key === this.ENTER_KEY) {
        this.handleKeyboardSelect();
        e.preventDefault();
        e.stopPropagation();
      } else if(e.key === this.DOWN_ARROW_KEY || e.key === this.TAB_KEY) {
        this.handleKeyboardDown();
        e.preventDefault();
        e.stopPropagation();
      } else if(e.key === this.UP_ARROW_KEY) {
        this.handleKeyboardUp();
        e.preventDefault();
        e.stopPropagation();
      }
    }
  }

  clickListener(e) {
    if(this.state.showSearchResults) {
      this.resetAutocompleteState();
      e.preventDefault();
      e.stopPropagation();
    }
  }

  handleKeyboardSelect() {
    if(this.state.focusedSearchResult >= 0) {
      this.searchResultElements[this.state.focusedSearchResult].click();
    }
  }

  handleKeyboardDown() {
    this.setState(prevState => {
      let newFocusedSearchResult;
      // first focus or the last element is focused
      if(prevState.focusedSearchResult === undefined || prevState.focusedSearchResult === (this.searchResultElements.length - 1)) {
        newFocusedSearchResult = 0;
      } else {
        newFocusedSearchResult = prevState.focusedSearchResult + 1;
      }
      return {
        focusedSearchResult: newFocusedSearchResult
      };
    }, () => {
      this.searchResultElements[this.state.focusedSearchResult].focus();
    });
  }

  handleKeyboardUp() {
    this.setState(prevState => {
      let newFocusedSearchResult;
      if(prevState.focusedSearchResult === undefined || prevState.focusedSearchResult === 0) {
        newFocusedSearchResult = this.searchResultElements.length - 1;
      } else {
        newFocusedSearchResult = prevState.focusedSearchResult - 1;
      }
      return {
        focusedSearchResult: newFocusedSearchResult
      };
    }, () => {
      this.searchResultElements[this.state.focusedSearchResult].focus();
    });
  }

  handleChange(e) {
    // update parent state with new value
    this.props.onChange(e);
    const newValue = e.target.value;

    // reset autocomplete when the last character in the input is in array
    const shouldResetForMultipleInputs = this.props.multipleInputs && [' ', ','].includes(newValue[newValue.length - 1]);

    // reset and hide listbox when there is no value in the input or it is ready for another value
    if(!newValue || shouldResetForMultipleInputs) {
      this.resetAutocompleteState();
    }

    // fetch autocomplete results if there is a string to search and no current pending search
    if(newValue && !shouldResetForMultipleInputs && !this.state.pendingSearch) {
      let query = newValue;
      if(this.props.multipleInputs) {
        const lastSpace = this.props.value.lastIndexOf(' ');
        const lastComma = this.props.value.lastIndexOf(',');
        if(lastSpace > 0 || lastComma > 0) {
          query = newValue.slice((lastSpace > lastComma ? lastSpace : lastComma) + 1);
        }
      }

      if(query) {
        const searchUrl = 'https://trunkclub-ui-takehome.now.sh/search/' + query;
        fetch(searchUrl).then(searchResponse => {
          if(searchResponse.ok) {
            searchResponse.json().then(searchData => {
              this.successfulAutocomplete(searchData.users);
            });
          } else {
            this.unsuccessfulAutocomplete();
          }
        });
        this.pendingAutocomplete();
      }
    }
  }

  searchResultHandleClick(e) {
    const autoCompleteValue = e.target.getAttribute('value');
    let newValue = autoCompleteValue;
    if(this.props.multipleInputs) {
      let lastSpace = this.props.value.lastIndexOf(' ');
      let lastComma = this.props.value.lastIndexOf(',');

      if(lastSpace > 0 || lastComma > 0) {
        // disregard when there is no space or comma
        lastSpace = lastSpace >= 0 ? lastSpace : 1000
        lastComma = lastComma >= 0 ? lastComma : 1000;
        newValue = this.props.value.slice(0, lastSpace < lastComma ? lastSpace : lastComma) + ',' + autoCompleteValue;
      }
    }
    // update parent state with autoComplete value
    this.props.onChange({target: {value: newValue}});
    this.resetAutocompleteState();
  }

  generateListboxContents() {
    let listboxContents;
    if(this.state.pendingSearch) {
      listboxContents = 'search pending';
    } else if(this.state.successfulSearch) {
      this.searchResultElements = [];
      listboxContents = this.state.searchValues.map((searchValue, index) => {
        return (
          <li
            className="result dropdown-item"
            tabIndex="0"
            key={searchValue.email}
            value={searchValue.email}
            ref={searchResult => { this.searchResultElements[index] = searchResult }}
          >
            {`${searchValue.firstName} ${searchValue.lastName} (${searchValue.email})`}
          </li>
        );
      });
    }
    return listboxContents;
  }

  render() {
    return (
      <div className="comboboxWrapper">
        <input
          type="text"
          name={this.props.name}
          id={this.props.id}
          className={this.props.className}
          value={this.props.value}
          onChange={this.handleChange}
          ref={input => { this.autocompleteInput = input }}
        />
        <ul
          className={`dropdown-menu ${this.state.showSearchResults ? 'show' : ''}`}
          onClick={this.searchResultHandleClick}
        >
          {this.generateListboxContents()}
        </ul>
      </div>
    );
  }
};

export default AutocompleteInput;
