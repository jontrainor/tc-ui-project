import React, { Component } from 'react';
import validate from 'validate.js'
import './App.css';
import AutocompleteInput from './AutocompleteInput'

class App extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.generateAlertMessage = this.generateAlertMessage.bind(this);
    this.toHandleChange = this.toHandleChange.bind(this);
    this.ccHandleChange = this.ccHandleChange.bind(this);
    this.subjectHandleChange = this.subjectHandleChange.bind(this);
    this.bodyHandleChange = this.bodyHandleChange.bind(this);

    this.emptyValues = {
      to: '',
      ccVisible: '',
      cc: [],
      subject: '',
      body: ''
    };

    this.emptyMeta = {
        to: {dirty: false, valid: undefined},
        cc: {dirty: false, valid: undefined},
        subject: {dirty: false, valid: undefined},
        body: {dirty: false, valid: undefined}
    };

    this.state = {
      values: this.emptyValues,
      meta: this.emptyMeta,
      submittedFormStatus: undefined
    };
  }

  toHandleChange(e) {
    const newValue = e.target.value;
    this.setState(prevState => {
      return {
        meta: {
          ...prevState.meta,
          to: {
            ...prevState.meta.to,
            dirty: true
          }
        },
        values: {
          ...prevState.values,
          to: newValue
        }
      }
    });
  }

  ccHandleChange(e) {
    const newValue = e.target.value;
    this.setState(prevState => {
      return {
        meta: {
          ...prevState.meta,
          cc: {
            ...prevState.meta.cc,
            dirty: true
          }
        },
        values: {
          ...prevState.values,
          ccVisible: newValue,
          cc: newValue.split(',').map(value => value.trim())
        }
      }
    });
  }

  subjectHandleChange(e) {
    const newValue = e.target.value;
    this.setState(prevState => {
      return {
        meta: {
          ...prevState.meta,
          subject: {
            ...prevState.meta.subject,
            dirty: true
          }
        },
        values: {
          ...prevState.values,
          subject: newValue
        }
      }
    });
  }

  bodyHandleChange(e) {
    const newValue = e.target.value;
    this.setState(prevState => {
      return {
        meta: {
          ...prevState.meta,
          body: {
            ...prevState.meta.body,
            dirty: true
          }
        },
        values: {
          ...prevState.values,
          body: newValue
        }
      }
    });
  }

  handleSubmit(e) {
    // validate.js used to get a standard email validation instead of having to write my own regex
    let isToValid = validate.single(this.state.values.to, {presence: true, email: true}) === undefined;
    let isCcValid = this.state.values.cc.every(cc => validate.single(cc, {presence: false, email: true}) === undefined);
    let isSubjectValid = this.state.values.subject.length > 0;
    let isBodyValid = this.state.values.body.length > 0;

    this.setState(prevState => {
      return {
        meta: {
          to: {
            valid: isToValid,
            dirty: true
          },
          cc: {
            valid: isCcValid,
            dirty: true
          },
          subject: {
            valid: isSubjectValid,
            dirty: true
          },
          body: {
            valid: isBodyValid,
            dirty: true
          }
        }
      }
    });

    if(isToValid && isCcValid && isSubjectValid && isBodyValid) {
      const body = {
        to: this.state.values.to,
        cc: this.state.values.cc,
        subject: this.state.values.subject,
        body: this.state.values.body
      }

      fetch('https://trunkclub-ui-takehome.now.sh/submit', {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify(body)
      }).then(submitResponse => {
        if(submitResponse.ok) {
          this.setState({
            values: this.emptyValues,
            meta: this.emptyMeta,
            submittedFormStatus: submitResponse.status
          });
        } else {
          this.setState({
            submittedFormStatus: submitResponse.status
          });
        }
      });
    }
    e.preventDefault();
    return false;
  }

  generateAlertMessage() {
    let alertMessage;
    const submittedFormStatus = this.state.submittedFormStatus;
    // the form has not been submitted yet
    if(!submittedFormStatus) {
      alertMessage = '';
    } else if(submittedFormStatus === 200) {
      alertMessage = <div className="alert alert-success">Your email has been successfully sent.</div>;
    } else if(submittedFormStatus === 400) {
      alertMessage = <div className="alert alert-warning">Form data is invalid. Please re-enter and try again.</div>;
    // handle 500 response and unexpected responses
    } else {
      alertMessage = <div className="alert alert-danger">An error occurred submitting your form. Please try again.</div>;
    }
    return alertMessage;
  }

  render() {
    return (
      <div className="App container">
        <header className="App-header">
          <h1 className="App-title">Trunk Club UI Project</h1>
        </header>
        {this.generateAlertMessage()}
        <form onSubmit={this.handleSubmit} autoComplete="off">
          <div className="form-group">
            <label forhtml="toInput">To:</label>
            <AutocompleteInput
              name="to"
              id="toInput"
              className={`form-control ${this.state.meta.to.dirty === true && this.state.meta.to.valid === false ? 'invalid' : ''}`}
              value={this.state.values.to}
              onChange={this.toHandleChange}
            />
          </div>
          <div className="form-group">
            <label forhtml="ccInput">cc:</label>
            <AutocompleteInput
              name="ccVisible"
              id="ccInput"
              className={`form-control ${this.state.meta.cc.dirty === true && this.state.meta.cc.valid === false ? 'invalid' : ''}`}
              multipleInputs={true}
              value={this.state.values.ccVisible}
              onChange={this.ccHandleChange}
            />
            <input type="hidden" value={this.state.values.cc} name="cc"></input>
          </div>
          <div className="form-group">
            <label forhtml="subjectInput">Subject:</label>
            <input
              type="text"
              name="subject"
              id="subjectInput"
              className={`form-control ${this.state.meta.subject.dirty === true && this.state.meta.subject.valid === false? 'invalid' : ''}`}
              value={this.state.values.subject}
              onChange={this.subjectHandleChange}
            />
          </div>
          <div className="form-group">
            <label forhtml="bodyInput">Body:</label>
            <textarea
              name="body"
              id="bodyInput"
              className={`form-control ${this.state.meta.body.dirty === true && this.state.meta.body.valid === false ? 'invalid' : ''}`}
              value={this.state.values.body}
              onChange={this.bodyHandleChange}
            />
          </div>
          <input type="submit" value="Submit" className="btn btn-primary"/>
        </form>
      </div>
    );
  }
}

export default App;
